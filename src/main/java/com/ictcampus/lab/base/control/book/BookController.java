package com.ictcampus.lab.base.control.book;

import com.ictcampus.lab.base.control.book.mapper.BookControllerStructMapper;
import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.service.book.BookService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
	@Autowired
	private BookService bookService;

	@Autowired
	private BookControllerStructMapper bookControllerStructMapper;

	@GetMapping(value = "", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> getBooks() {
		log.debug("Fetching all books");
		List<BookResponse> books = bookControllerStructMapper.toBooks(bookService.getBooks());
		log.debug("Fetched {} books", books.size());
		return books;
	}

	@GetMapping(value = "/custom", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> getBooksCustom() {
		log.debug("Fetching all books with extended data");
		List<BookResponse> books = bookControllerStructMapper.toBooks(bookService.getBooksCustom());
		log.debug("Fetched {} books with extended data", books.size());
		return books;
	}

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public BookResponse getBookById(@PathVariable Long id) {
		log.debug("Fetching book with ID: {}", id);
		BookResponse bookResponse = bookControllerStructMapper.toBookResponse(bookService.getBookById(id));
		log.debug("Fetched book: {}", bookResponse);
		return bookResponse;
	}

	@GetMapping(value = "/title/{title}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public BookResponse getBookByTitle(@PathVariable String title) {
		log.debug("Fetching book with title: {}", title);
		BookResponse bookResponse = bookControllerStructMapper.toBookResponse(bookService.getBookByTitle(title));
		log.debug("Fetched book: {}", bookResponse);
		return bookResponse;
	}

	@GetMapping(value = "/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> searchBooks(@RequestParam String searchString) {
		log.debug("Searching books with search string: {}", searchString);
		List<BookResponse> books = bookControllerStructMapper.toBooks(bookService.searchBooks(searchString));
		log.debug("Found {} books matching search string: {}", books.size(), searchString);
		return books;
	}

	@PostMapping(value = "", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BookResponse createBook(@RequestBody BookResponse bookResponse) {
		log.debug("Creating book with details: {}", bookResponse);
		BookResponse createdBookResponse = bookControllerStructMapper.toBookResponse(
				bookService.createBook(bookControllerStructMapper.toBook(bookResponse)));
		log.debug("Created book: {}", createdBookResponse);
		return createdBookResponse;
	}
}
