package com.ictcampus.lab.base.control.user;

import com.ictcampus.lab.base.repository.user.entity.UserEntity;
import com.ictcampus.lab.base.service.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public UserEntity registerUser(@RequestBody UserEntity userEntity) {
        return userService.registerUser(userEntity);
    }
}