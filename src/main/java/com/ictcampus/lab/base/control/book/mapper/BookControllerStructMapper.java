package com.ictcampus.lab.base.control.book.mapper;

import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.service.book.model.Book;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper
public interface BookControllerStructMapper {
	List<BookResponse> toBooks(List<Book> books);
	BookResponse toBookResponse(Book book);
	Book toBook(BookResponse bookResponse);
}
