package com.ictcampus.lab.base.service.book.model;

import lombok.Value;

@Value
public class Position {
	private Long id;
	private String floor;
	private String sector;
	private String rack;
	private String line;
	private Book book;
}