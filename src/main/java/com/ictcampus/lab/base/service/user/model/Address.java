package com.ictcampus.lab.base.service.user.model;

import lombok.Value;

import java.util.List;

@Value
public class Address {
    private Long id;
    private String name;
    private String street;
    private String cap;
    private String city;
    private String province;
    private List<User> users;
}
