package com.ictcampus.lab.base.service.book.model;

import lombok.Data;
import java.time.LocalDate;
import java.util.List;

@Data
public class Author {
	private Long id;
	private String name;
	private String surname;
	private String nickname;
	private LocalDate birthday;
	private List<Book> books;


	 /*
    public void addBook(Book book) {
        if (!this.books.contains(book)) {
            this.books.add(book);
            book.addAuthor(this);  // Aggiunge l'autore al libro mantenendo la bidirezionalità
        }
    }

    public void removeBook(Book book) {
        if (this.books.contains(book)) {
            this.books.remove(book);
            book.removeAuthor(this);  // Rimuove l'autore dal libro mantenendo la bidirezionalità
        }
    }
    */
}

