package com.ictcampus.lab.base.service.book;

import com.ictcampus.lab.base.repository.book.BookRepository;
import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.mapper.BookServiceStructMapper;
import com.ictcampus.lab.base.service.book.model.Book;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
@Slf4j
public class BookService {
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private BookServiceStructMapper bookServiceStructMapper;

	public List<Book> getBooks() {
		log.debug("Retrieving all books from repository");
		return bookRepository.findAll().stream()
				.map(bookServiceStructMapper::toBook)
				.collect(Collectors.toList());
	}

	public List<Book> getBooksCustom() {
		log.debug("Retrieving all books with extended data from repository");
		return bookRepository.findBooksWithExtendData().stream()
				.map(bookServiceStructMapper::toBook)
				.collect(Collectors.toList());
	}

	public Book getBookById(Long id) {
		log.debug("Retrieving book by ID: {}", id);
		return bookRepository.findById(id)
				.map(bookServiceStructMapper::toBook)
				.orElseThrow(() -> new IllegalArgumentException("Book not found with id: " + id));
	}

	public Book getBookByTitle(String title) {
		log.debug("Retrieving book by title: {}", title);
		BookEntity bookEntity = bookRepository.findByTitle(title);
		if (bookEntity != null) {
			return bookServiceStructMapper.toBook(bookEntity);
		} else {
			throw new IllegalArgumentException("Book not found with title: " + title);
		}
	}

	public List<Book> searchBooks(String searchString) {
		log.debug("Searching books with search string: {}", searchString);
		return bookRepository.searchBooksByTitleOrAuthor(searchString).stream()
				.map(bookServiceStructMapper::toBook)
				.collect(Collectors.toList());
	}

	public Book createBook(Book book) {
		log.debug("Creating a new book with details: {}", book);
		return bookServiceStructMapper.toBook(bookRepository.save(bookServiceStructMapper.toBookEntity(book)));
	}
}
