package com.ictcampus.lab.base.service.user.model;

import com.ictcampus.lab.base.service.user.UserRole;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Data
public class User {
    private Long id;
    private String username;
    private String password;
    private String name;
    private String surname;
    private final UserRole role;
    private List<Address> addresses;
}
